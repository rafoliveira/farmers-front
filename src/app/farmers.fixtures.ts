import { Farmer } from './shared/farmer';
import { Document } from './shared/document';
import { Address } from './shared/address';


const addresses:Address[] = [
  {
    street: 'calle X #60a-03',
    state: 'Bogota',
    country: 'CO',
  },
  {
    street: 'calle 01',
    state: 'Bogota',
    country: 'CO',
  },
  {
    street: 'calle 02',
    state: 'Bogota',
    country: 'CO',
  },

  ];
const documents: Document[] = [
  {
    documentNumber: 'A12233',
    documentType: 'ID',
  },
  {
    documentNumber: 'B3344',
    documentType: 'ID',
  },
  {
    documentNumber: 'C4455',
    documentType: 'ID',
  }
];


export const FARMERS: Farmer[] = [
  {
    id: '1',
    name: 'Rafael Oliveira',
    document: documents[0],
    address: addresses[0],
  },
  {
    id: '2',
    name: 'Carolina Suica',
    document: documents[1],
    address: addresses[1],
  },
  {
    id: '3',
    name: 'Limao Leite',
    document: documents[2],
    address: addresses[2],
  }
];
