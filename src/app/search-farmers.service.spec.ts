import { TestBed } from '@angular/core/testing';

import { SearchFarmersService } from './search-farmers.service';

describe('SearchFarmersService', () => {
  let service: SearchFarmersService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SearchFarmersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
