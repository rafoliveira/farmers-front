import { Component, OnInit } from '@angular/core';
import { Farmer, blankFarmer } from '../shared/farmer';
import { SearchFarmersService } from '../search-farmers.service';
import { Observable, Subject } from 'rxjs';

import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';

@Component({
  selector: 'app-farmer-search-card',
  templateUrl: './farmer-search-card.component.html',
  styleUrls: ['./farmer-search-card.component.scss']
})
export class FarmerSearchCardComponent implements OnInit {
  searchTerms = new Subject<string>();
  farmer: Farmer = blankFarmer;

  constructor(private searchFarmersService: SearchFarmersService) { }

  ngOnInit(): void {
    this.searchTerms.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      switchMap((term: string) => this.searchFarmersService.search(term))
    ).subscribe(farmers => this.setFarmer(farmers[0]));
  }

  private setFarmer(farmer){
    if (farmer){
      this.farmer = farmer;
    }else{
      this.farmer = blankFarmer;
    }
  }

  search(term: string): void {
    this.searchTerms.next(term);
  }

}
