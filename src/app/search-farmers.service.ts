import { Injectable } from '@angular/core';
import { FARMERS } from './farmers.fixtures';
import { Farmer, blankFarmer } from './shared/farmer';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SearchFarmersService {
  // TODO: add url to config
  private apiUrl = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  search(term: string): Observable<Farmer[]> {
    if (!term.trim()){
      return of([blankFarmer]);
    }

    return this.http.get<Farmer[]>(`${this.apiUrl}/farmers?q=${term}`).pipe();
  }
}
