import { Address } from './address';
import { Document } from './document';

export interface Farmer {
  id: string;
  document: Document;
  name: string;
  address: Address;
}

export const blankFarmer: Farmer = {
  id: '',
  name: '',
  document: {
    documentNumber: '',
    documentType: ''
  },
  address: {
    street: '',
    state: '',
    country: ''
  }
};
