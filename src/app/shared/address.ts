export interface Address {
  street: string;
  state: string;
  country: string;
}
